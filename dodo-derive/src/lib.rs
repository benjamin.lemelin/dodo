//! # Dodo-Derive
//!
//! This crate provides Dodo's derive macros.
use proc_macro::TokenStream;

use quote::quote;
use syn::{Data, DataStruct, DeriveInput, Fields};

/// Implements the `Entity` trait for your struct. It must contain a field
/// named `id` of type `Option<Uuid>`.
#[proc_macro_derive(Entity)]
pub fn entity_derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = syn::parse(input).unwrap();

    if let Data::Struct(DataStruct { fields: Fields::Named(ref fields), .. }) = &ast.data {
        if fields.named.iter().any(|field| {
            match field.ident {
                Some(ref ident) => ident.to_string() == "id",
                None => false
            }
        }) {
            impl_entity(&ast)
        } else {
            panic!("Entity derive must be used on a struct with a field named \"id\".")
        }
    } else {
        panic!("Entity derive must be used on a struct with named fields.")
    }
}

fn impl_entity(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let expanded = quote! {
        impl dodo::Entity for #name {
            fn id(&self) -> Option<uuid::Uuid> { self.id }
            fn set_id(&mut self, id: Option<uuid::Uuid>) { self.id = id }
        }
    };

    TokenStream::from(expanded)
}