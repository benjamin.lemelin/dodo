use std::error::Error;

use serde::{Deserialize, Serialize};
use uuid::Uuid;

use dodo::prelude::*;

#[derive(Debug, Entity, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Person {
    id: Option<Uuid>,
    name: String,
    age: u64,
}

//You should create a type for shorter names
type PersonCollection = Collection<Person, Directory, JsonSerializer>;
type NameIndex = Index<String, Directory, JsonSerializer>;

fn main() -> Result<(), Box<dyn Error>> {
    // Create collection and index
    // Make sure collection and index are on separate storages
    let mut collection = PersonCollection::new(Directory::new("./person/collection")?);
    let mut index = NameIndex::new(Directory::new("./person/index")?);

    //Clear collection and index
    collection.clear()?;
    index.clear()?;

    // Add persons to collection and to index
    let mut person1 = Person { id: None, name: "John Smith".into(), age: 42 };
    collection.insert(&mut person1)?;
    index.add(person1.id.unwrap(), &person1.name)?;

    let mut person2 = Person { id: None, name: "John Smith".into(), age: 17 };
    collection.insert(&mut person2)?;
    index.add(person2.id.unwrap(), &person2.name)?;

    let mut person3 = Person { id: None, name: "Mary Smith".into(), age: 21 };
    collection.insert(&mut person3)?;
    index.add(person3.id.unwrap(), &person3.name)?;

    // Find all "John Smith"s within index
    let _ids = index.find("John Smith")?;

    // Find all names within index
    let _names: Vec<String> = index.find_all()?.map(|(_ids, name)| name).collect();

    // Remove all "John Smiths" from collection and index
    for id in index.remove_value("John Smith")? {
        collection.delete(id)?;
    }

    // Remove "Mary Smith" from collection and index
    index.remove_id(person3.id.unwrap())?;
    collection.delete(person3.id.unwrap())?;

    Ok(())
}