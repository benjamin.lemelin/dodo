use std::error::Error;

use serde::{Deserialize, Serialize};
use uuid::Uuid;

use dodo::prelude::*;

//The "Entity" derive implements the "Entity" trait for you.
#[derive(Debug, Entity, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Person {
    //Every entity must have an "id" field of type "Option<Uuid>".
    id: Option<Uuid>,
    name: String,
    age: u64,
}

//You should create a type for shorter names
type PersonCollection = Collection<Person, Directory, JsonSerializer>;

fn main() -> Result<(), Box<dyn Error>> {
    // Create collection
    let mut collection = PersonCollection::new(Directory::new("./person/collection")?);

    //Clear collection
    collection.clear()?;

    // Add a person
    let mut person = Person { id: None, name: "John Smith".into(), age: 42 };
    collection.insert(&mut person)?;

    // Get a person
    let id = person.id.unwrap();
    let mut person = collection.find(id)?;

    // Find persons older than 20
    let _persons = collection.find_all()?.filter(|person| person.age > 20).collect()?;

    // Update a person
    person.name = "Mary Smith".into();
    collection.update(&person)?;

    // Delete a person
    collection.delete(person.id.unwrap())?;
    Ok(())
}