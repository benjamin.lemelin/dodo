use serde::{Deserialize, Serialize};
use uuid::Uuid;

use dodo::prelude::*;

#[derive(Clone, Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Person {
    pub id: Option<Uuid>,
    pub name: String,
    pub age: u64,
}

impl Person {
    #[allow(dead_code)]
    pub fn new() -> Self {
        Self {
            id: None,
            name: "John Smith".into(),
            age: 42,
        }
    }

    #[allow(dead_code)]
    pub fn with_id(id: Uuid) -> Self {
        Self {
            id: Some(id),
            name: "John Smith".into(),
            age: 42,
        }
    }

    #[allow(dead_code)]
    pub fn with_age(age: u64) -> Self {
        Self {
            id: None,
            name: "John Smith".into(),
            age,
        }
    }
}