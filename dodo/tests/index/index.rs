use uuid::Uuid;

use dodo::Memory;
use std::collections::HashSet;

#[test]
fn can_create_index() {
    fixture::Index::new(Memory::new());
}

#[test]
fn can_create_index_with_id() {
    let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();

    fixture::Index::with_id(Memory::new(), id);
}

#[test]
fn can_find_with_value() {
    let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    let value = "John Smith".into();
    let mut index = fixture::Index::new(Memory::new());
    index.add(id, &value).expect("could not add to index");

    let ids = index.find(&value).expect("could not read index");
    assert!(ids.contains(&id), "id should have been in index");
}

#[test]
fn cant_find_absent_value() {
    let index = fixture::Index::new(Memory::new());

    assert!(index.find("John Smith").is_err(), "value should not exist");
}

#[test]
fn can_find_all_values() {
    let mut index = fixture::Index::new(Memory::new());

    let id1 = Uuid::new_v4();
    let value1 = "John Smith".into();
    index.add(id1, &value1).expect("could not add to index");
    let id2 = Uuid::new_v4();
    let value2 = "Mary Smith".into();
    index.add(id2, &value2).expect("could not add to index");

    let entries: Vec<(HashSet<Uuid>, String)> = index.find_all().expect("index should be readable").collect();

    assert!(entries.iter().any(|(ids, name)| ids.contains(&id1) && *name == *value1), "id and value should have been in index");
    assert!(entries.iter().any(|(ids, name)| ids.contains(&id2) && *name == *value2), "id and value should have been in index");
}

#[test]
fn can_add_value_to_index() {
    let mut index = fixture::Index::new(Memory::new());

    let id = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id, &value).expect("could not add to index");

    assert!(index.find(&value).expect("could not read index").contains(&id), "value should have been in index");
}

#[test]
fn should_not_create_duplicates() {
    let mut index = fixture::Index::new(Memory::new());

    let id = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id, &value).expect("could not add to index");
    index.add(id, &value).expect("could not add to index");

    let entries: Vec<(HashSet<Uuid>, String)> = index.find_all().expect("could not read index").collect();

    assert_eq!(entries.len(), 1, "should not have any duplicated value in index");
    assert_eq!(entries[0].0.len(), 1, "should not have any duplicated keys in index");
}

#[test]
fn can_remove() {
    let mut index = fixture::Index::new(Memory::new());
    let id = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id, &value).expect("could not add to index");

    let removed = index.remove(id, &value).expect("could not remove from index");

    assert!(removed, "something should have been removed");
    assert!(index.find(&value).is_err(), "value should not exist");
}

#[test]
fn can_remove_absent() {
    let mut index = fixture::Index::new(Memory::new());
    let id = Uuid::new_v4();
    let value: String = "John Smith".into();

    let removed = index.remove(id, &value).expect("could not remove from index");

    assert!(!removed, "nothing should have been removed");
    assert!(index.find_all().expect("could not read index").collect::<Vec<(HashSet<Uuid>, String)>>().is_empty(), "index should have been empty");
}

#[test]
fn should_not_remove_value_if_id_remains() {
    let mut index = fixture::Index::new(Memory::new());

    let id1 = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id1, &value).expect("could not add to index");
    let id2 = Uuid::new_v4();
    index.add(id2, &value).expect("could not add to index");

    let removed = index.remove(id1, &value).expect("could not remove from index");

    assert!(removed, "something should have been removed");
    assert!(index.find(&value).expect("could not read index").contains(&id2), "value should exist");
}

#[test]
fn should_remove_value_if_no_id_remains() {
    let mut index = fixture::Index::new(Memory::new());

    let id1 = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id1, &value).expect("could not add to index");
    let id2 = Uuid::new_v4();
    index.add(id2, &value).expect("could not add to index");

    index.remove(id1, &value).expect("could not remove from index");
    index.remove(id2, &value).expect("could not remove from index");

    assert!(index.find(&value).is_err(), "value should not exist");
}

#[test]
fn can_remove_id() {
    let mut index = fixture::Index::new(Memory::new());
    let id = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id, &value).expect("could not add to index");

    let removed = index.remove_id(id).expect("could not remove from index");

    assert!(removed, "something should have been removed");
    assert!(index.find(&value).is_err(), "value should not exist");
}

#[test]
fn can_remove_absent_id() {
    let mut index = fixture::Index::new(Memory::new());
    let id = Uuid::new_v4();

    let removed = index.remove_id(id).expect("could not remove from index");

    assert!(!removed, "nothing should have been removed");
    assert!(index.find_all().expect("could not read index").collect::<Vec<(HashSet<Uuid>, String)>>().is_empty(), "index should have been empty");
}

#[test]
fn can_remove_value() {
    let mut index = fixture::Index::new(Memory::new());
    let id = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id, &value).expect("could not add to index");

    let removed = index.remove_value(&value).expect("could not remove from index");

    assert!(removed.contains(&id), "id should have been removed");
    assert!(index.find(&value).is_err(), "value should not exist");
}

#[test]
fn should_remove_all_ids_of_value() {
    let mut index = fixture::Index::new(Memory::new());

    let id1 = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id1, &value).expect("could not add to index");
    let id2 = Uuid::new_v4();
    index.add(id2, &value).expect("could not add to index");

    let removed = index.remove_value(&value).expect("could not remove from index");

    assert!(removed.contains(&id1), "id should have been removed");
    assert!(removed.contains(&id2), "id should have been removed");
    assert!(index.find(&value).is_err(), "value should not exist");
}

#[test]
fn can_remove_absent_value() {
    let mut index = fixture::Index::new(Memory::new());

    let removed = index.remove_value("John Smith").expect("could not remove from index");

    assert!(removed.is_empty(), "nothing should have been removed");
    assert!(index.find_all().expect("could not read index").collect::<Vec<(HashSet<Uuid>, String)>>().is_empty(), "index should have been empty");
}

#[test]
fn can_clear_index() {
    let mut index = fixture::Index::new(Memory::new());

    let id1 = Uuid::new_v4();
    let value = "John Smith".into();
    index.add(id1, &value).expect("could not add to index");
    let id2 = Uuid::new_v4();
    index.add(id2, &value).expect("could not add to index");

    index.clear().expect("could not clear index");

    assert!(index.find_all().expect("could not read index").collect::<Vec<(HashSet<Uuid>, String)>>().is_empty(), "index should have been empty");
}

#[test]
fn can_clear_empty_index() {
    let mut index = fixture::Index::new(Memory::new());

    index.clear().expect("could not clear index");

    assert!(index.find_all().expect("could not read index").collect::<Vec<(HashSet<Uuid>, String)>>().is_empty(), "index should have been empty");
}

mod fixture {
    use dodo::{Memory, prelude::*};

    pub type Index = dodo::Index<String, Memory, JsonSerializer>;
}