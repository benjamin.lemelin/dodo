use dodo::{Serializer, YamlSerializer};

use crate::common::Person;

#[test]
fn can_serialize_to_yaml() {
    let mut file: Vec<u8> = Vec::new();
    let input_data = Person::new();

    YamlSerializer::serialize(&mut file, &input_data).expect("could not serialize");
    let output_data: Person = YamlSerializer::deserialize(&*file).expect("could not deserialize");

    assert_eq!(input_data, output_data, "deserialized data is not the same as the serialized data");
}