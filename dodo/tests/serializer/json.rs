use dodo::{JsonSerializer, Serializer};

use crate::common::Person;

#[test]
fn can_serialize_to_json() {
    let mut file: Vec<u8> = Vec::new();
    let input_data = Person::new();

    JsonSerializer::serialize(&mut file, &input_data).expect("could not serialize");
    let output_data: Person = JsonSerializer::deserialize(&*file).expect("could not deserialize");

    assert_eq!(input_data, output_data, "deserialized data is not the same as the serialized data");
}