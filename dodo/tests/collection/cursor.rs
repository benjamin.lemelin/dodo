use dodo::{Memory, prelude::*};

use crate::common::Person;

#[test]
fn can_skip_entities() {
    let mut collection = fixture::Collection::new(Memory::new());
    fixture::create_entity_in(&mut collection);
    fixture::create_entity_in(&mut collection);

    let entities = collection
        .find_all().expect("could not read collection")
        .skip(1)
        .collect().expect("could not read collection");

    assert_eq!(entities.len(), 1, "only one entity should have been yielded");
}

#[test]
fn can_filter_entities() {
    let mut collection = fixture::Collection::new(Memory::new());
    let mut entity1 = Person::with_age(42);
    let mut entity2 = Person::with_age(18);
    fixture::add_entity_in(&mut collection, &mut entity1);
    fixture::add_entity_in(&mut collection, &mut entity2);

    let entities = collection
        .find_all().expect("could not read collection")
        .filter(|person| person.age > 20)
        .collect().expect("could not read collection");

    assert!(entities.contains(&entity1), "entity 1 should be yielded");
    assert!(!entities.contains(&entity2), "entity 2 should have been filtered");
}

#[test]
fn can_map_entities() {
    let mut collection = fixture::Collection::new(Memory::new());
    let mut entity1 = Person::with_age(42);
    let mut entity2 = Person::with_age(18);
    fixture::add_entity_in(&mut collection, &mut entity1);
    fixture::add_entity_in(&mut collection, &mut entity2);

    let entities = collection
        .find_all().expect("could not read collection")
        .map(|person| person.age)
        .collect().expect("could not read collection");

    assert!(entities.contains(&42), "entity 1 age should be yielded");
    assert!(entities.contains(&18), "entity 2 age should be yielded");
}

#[test]
fn can_get_first_entity() {
    let mut collection = fixture::Collection::new(Memory::new());
    let (_, expected_entity) = fixture::create_entity_in(&mut collection);

    let actual_entity = collection
        .find_all().expect("could not read collection")
        .first().expect("could not read collection").expect("entity should exist");

    assert_eq!(expected_entity, actual_entity, "entities should be the same.");
}

#[test]
fn can_get_nth_entity() {
    let mut collection = fixture::Collection::new(Memory::new());
    fixture::create_entity_in(&mut collection);
    fixture::create_entity_in(&mut collection);

    let mut cursor = collection.find_all().expect("could not read collection");

    assert!(matches!(cursor.nth(1), Ok(Some(_person))), "should yield second entity");
    assert!(matches!(cursor.next(), Ok(None)), "cursor should be empty.");
}

#[test]
fn can_take_n_entities() {
    let mut collection = fixture::Collection::new(Memory::new());
    fixture::create_entity_in(&mut collection);
    fixture::create_entity_in(&mut collection);

    let entities = collection
        .find_all().expect("could not read collection")
        .take(1).expect("could not read collection");

    assert_eq!(entities.len(), 1, "should yield only one entity");
}

mod fixture {
    use uuid::Uuid;

    use dodo::{Memory, prelude::*};

    use crate::common::Person;

    pub type Collection = dodo::Collection<Person, Memory, JsonSerializer>;

    pub fn create_entity_in(collection: &mut Collection) -> (Uuid, Person) {
        let mut entity = Person::new();
        collection.insert(&mut entity).expect("could not insert in collection");
        let id = entity.id.expect("unable to get id from entity");
        (id, entity)
    }

    pub fn add_entity_in(collection: &mut Collection, mut entity: &mut Person) {
        collection.insert(&mut entity).expect("could not insert in collection");
    }
}