//! Entity.

use serde::{de::DeserializeOwned, Serialize};
use uuid::Uuid;

/// Entity.
///
/// Entities are identified by a [Uuid](https://docs.rs/uuid/*/uuid/struct.Uuid.html), chosen by
/// a collection. Having no id (i.e. `None`) usually means it is not persisted.
///
/// Entities are required to be [serializable](https://docs.serde.rs/serde/trait.Serialize.html)
/// and [deserializable](https://docs.serde.rs/serde/de/trait.DeserializeOwned.html) in order to be
/// persisted.
///
/// # Example
///
/// ```
/// use dodo::prelude::*;
/// use serde::{Deserialize, Serialize};
/// use uuid::Uuid;
///
/// #[derive(Serialize, Deserialize)]
/// #[serde(rename_all = "camelCase")]
/// pub struct Person {
///     id: Option<Uuid>,
///     name: String,
///     age: u64,
/// }
///
/// impl Entity for Person {
///     fn id(&self) -> Option<Uuid> { self.id }
///     fn set_id(&mut self,id: Option<Uuid>) { self.id = id }
/// }
/// ```
///
/// # Derive
///
/// Use the `Entity` derive to quickly implement this trait.
///
/// ```
/// use dodo::prelude::*;
/// use serde::{Deserialize, Serialize};
/// use uuid::Uuid;
///
/// #[derive(Entity, Serialize, Deserialize)]
/// #[serde(rename_all = "camelCase")]
/// pub struct Person {
///     id: Option<Uuid>,
///     name: String,
///     age: u64,
/// }
/// ```
pub trait Entity: Serialize + DeserializeOwned {
    /// Id.
    fn id(&self) -> Option<Uuid>;

    /// Set the id.
    fn set_id(&mut self, id: Option<Uuid>);
}