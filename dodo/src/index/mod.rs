//! Index of values.

use std::borrow::Borrow;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::marker::PhantomData;
use std::str::FromStr;
use std::vec;

use uuid::Uuid;

pub use error::*;

use crate::serializer::Serializer;
use crate::storage::Storage;

mod error;

type IndexDocument = HashMap<String, HashSet<Uuid>>;

/// Index of values.
///
/// Indexes are usually paired with collections to make some queries faster. They allow to quickly
/// locate an entity id without having to search the whole collection.
///
/// # Example
///
/// ```
/// use dodo::prelude::*;
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, name : String, age: u64 }
/// #
/// # impl Person {
/// #    fn new(name : &str) -> Self { Self { id : None, name : name.into(), age : 42 } }
/// # }
///
/// type PersonCollection = Collection<Person, Directory, JsonSerializer>;
/// type NameIndex = Index<String, Directory, JsonSerializer>;
///
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #   let collection_path  = tempfile::tempdir()?;
/// #   let index_path  = tempfile::tempdir()?;
///     let mut collection = PersonCollection::new(Directory::new(&collection_path)?);
///     let mut index = NameIndex::new(Directory::new(&index_path)?);
///
///     let mut person1 = Person::new("John Smith");
///     collection.insert(&mut person1)?;
///     index.add(person1.id.unwrap(), &person1.name)?;
///     let mut person2 = Person::new("John Smith");
///     collection.insert(&mut person2)?;
///     index.add(person2.id.unwrap(), &person2.name)?;
///     let mut person3 = Person::new("Mary Smith");
///     collection.insert(&mut person3)?;
///     index.add(person3.id.unwrap(), &person3.name)?;
///
///     // Ids of all "John Smith"s.
///     let ids = index.find("John Smith")?;
///
///     println!("{:?}", ids);
///
///     Ok(())
/// }
/// ```
///
/// # Storage optimisations
///
/// Like collections, indexes assume to have complete control of their storage, but if needed, they
/// can share a common storage as long as each index have a different id assigned to it. See
/// the `with_index` function for details.
///
/// # Serializer
///
/// Like collections, indexes need a serializer to serializer the index data.
///
/// # Other considerations
///
/// You should only index basic types, like numbers or strings. You can still index any value
/// as long as it implements [Hash](https://doc.rust-lang.org/stable/std/hash/trait.Hash.html),
/// [ToString](https://doc.rust-lang.org/stable/std/string/trait.ToString.html) and
/// [FromStr](https://doc.rust-lang.org/stable/std/str/trait.FromStr.html), but it should be
/// considerez bad practice.
#[derive(Debug, Clone)]
pub struct Index<T, S, R> {
    id: Uuid,
    storage: S,
    _t: PhantomData<T>,
    _r: PhantomData<R>,
}

impl<T, S, R> Index<T, S, R>
    where S: Storage,
          R: Serializer {
    /// Create a new index, using provided storage.
    ///
    /// When created like this, the index assumes that it has complete control over the storage
    /// and stores the index data a single file named `00000000-0000-0000-0000-000000000000`. If
    /// you want to reuse a storage for multiple indexes, see the `with_id` function.
    ///
    /// # Examples
    ///
    /// ```
    /// use dodo::prelude::*;
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, name : String, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new(name : &str) -> Self { Self { id : None, name : name.into(), age : 42 } }
    /// # }
    ///
    /// type NameIndex = Index<String, Directory, JsonSerializer>;
    ///
    /// fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #   let index_path  = tempfile::tempdir()?;
    ///     let mut index = NameIndex::new(Directory::new(&index_path)?);
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn new(storage: S) -> Self {
        Self::with_id(storage, Uuid::nil())
    }

    /// Create a new index inside provided storage, using assigned id.
    ///
    /// When created like this, the index knows that it does not have complete control over the
    /// storage and stores the index data a single file named after his assigned id.
    ///
    /// # Examples
    ///
    /// ```
    /// use dodo::prelude::*;
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, name : String, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new(name : &str) -> Self { Self { id : None, name : name.into(), age : 42 } }
    /// # }
    ///
    /// type NameIndex = Index<String, Directory, JsonSerializer>;
    ///
    /// fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #   let index_path  = tempfile::tempdir()?;
    ///     let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    ///     let mut index = NameIndex::with_id(Directory::new(&index_path)?, id);
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn with_id(storage: S, id: Uuid) -> Self {
        Self {
            id,
            storage,
            _t: PhantomData,
            _r: PhantomData,
        }
    }

    /// Returns all ids corresponding to the given value.
    ///
    /// Returns an error if not found.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type NameIndex = Index<String, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let index = NameIndex::new(Memory::new());
    /// #
    /// let ids = index.find("John Smith");
    ///
    /// match ids {
    ///     Ok(ids) => println!("Found!"),
    ///     Err(e) if e.is_not_found() => println!("Not found!"),
    ///     Err(_) => println!("Other error!")
    /// }
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn find<Q>(&self, value: &Q) -> Result<HashSet<Uuid>>
        where T: Borrow<Q>,
              Q: Hash + ToString + ?Sized {
        let mut index_document = self.read()?;

        let value = value.to_string();
        index_document.remove(&value).ok_or_else(|| IndexError::not_found())
    }

    /// Provide an iterator through the index entries.
    ///
    /// This can be pretty useful to query all index values.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// # use std::collections::HashSet;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type NameIndex = Index<String, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let index = NameIndex::new(Memory::new());
    /// #
    /// let names: Vec<String> = index.find_all()?.map(|(ids, name) : (HashSet<Uuid>, String)| name).collect();
    ///
    /// println!("{:#?}", names);
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn find_all(&self) -> Result<IndexIterator<T>>
        where T: FromStr {
        IndexIterator::new(self.read()?)
    }

    /// Add value to the index, assigned to provided id.
    ///
    /// The index doesn't keep multiple copies of the same key/value pair (i.e there are no
    /// duplicates).
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// # use std::collections::HashSet;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type NameIndex = Index<String, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut index = NameIndex::new(Memory::new());
    /// #
    /// let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    /// let value = "John Smith".into();
    ///
    /// index.add(id, &value)?;
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn add(&mut self, id: Uuid, value: &T) -> Result<()>
        where T: Hash + ToString {
        let mut index_document = self.read()?;

        let mut has_changed = false;
        has_changed = has_changed || index_document.entry(value.to_string()).or_insert_with(|| {
            has_changed = true;
            Default::default()
        }).insert(id);

        if has_changed {
            self.write(index_document)?;
        }
        Ok(())
    }

    /// Remove id/value pair from index.
    ///
    /// This does not fail if the pair doesn't exist in the index. Instead, this returns a
    /// boolean : if true, the pair was removed, and if false, the pair was not found.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// # use std::collections::HashSet;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type NameIndex = Index<String, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut index = NameIndex::new(Memory::new());
    /// #
    /// let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    /// let value : String = "John Smith".into();
    /// index.add(id, &value)?;
    ///
    /// assert!(index.remove(id, &value)?);
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn remove<Q>(&mut self, id: Uuid, value: &Q) -> Result<bool>
        where T: Borrow<Q>,
              Q: Hash + ToString + ?Sized {
        let mut index_document = self.read()?;

        let value = value.to_string();
        let mut has_changed = false;
        if let Some(ids) = index_document.get_mut(&value) {
            has_changed = ids.remove(&id);
            if ids.is_empty() { index_document.remove(&value); }
        }

        if has_changed { self.write(index_document)?; }
        Ok(has_changed)
    }

    /// Remove id from index.
    ///
    /// This has some serious performance implications as it must traverse the entire index and
    /// should only be used when unavoidable.
    ///
    /// This does not fail if the id doesn't exist in the index. Instead, this returns a boolean :
    /// if true, the id was removed, and if false, the id was not found.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// # use std::collections::HashSet;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type NameIndex = Index<String, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut index = NameIndex::new(Memory::new());
    /// #
    /// let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    /// let value : String = "John Smith".into();
    /// index.add(id, &value)?;
    ///
    /// assert!(index.remove_id(id)?);
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn remove_id(&mut self, id: Uuid) -> Result<bool> {
        let mut index_document = self.read()?;

        let mut has_changed = false;
        index_document.retain(|_, it| {
            has_changed = has_changed || it.remove(&id);
            it.len() > 0
        });

        if has_changed { self.write(index_document)?; }
        Ok(has_changed)
    }

    /// Remove value from index and all related ids.
    ///
    /// Returns the ids assigned to the removed value. Note that theses ids can still be in the
    /// index, but assigned to others values.
    ///
    /// This does not fail if the value doesn't exist in the index. In that case, this returns an
    /// empty set of ids.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// # use std::collections::HashSet;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type NameIndex = Index<String, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut index = NameIndex::new(Memory::new());
    /// #
    /// let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    /// let value : String = "John Smith".into();
    /// index.add(id, &value)?;
    ///
    /// let ids = index.remove_value(&value)?;
    /// assert!(ids.contains(&id));
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn remove_value<Q>(&mut self, value: &Q) -> Result<HashSet<Uuid>>
        where T: Borrow<Q>,
              Q: Hash + ToString + ?Sized {
        let mut index_document = self.read()?;

        match index_document.remove(&value.to_string()) {
            Some(ids) => {
                self.write(index_document)?;
                Ok(ids)
            }
            None => {
                Ok(Default::default())
            }
        }
    }

    /// Remove every value in this index.
    ///
    /// Everything in this index will be deleted. Use at your own risks.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// # use std::collections::HashSet;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type NameIndex = Index<String, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut index = NameIndex::new(Memory::new());
    /// #
    /// #     let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    /// #     let value = "John Smith".into();
    /// #
    /// index.add(id, &value)?;
    ///
    /// index.clear()?;
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn clear(&mut self) -> Result<()> {
        self.write(Default::default())
    }

    fn read(&self) -> Result<IndexDocument> {
        match self.storage.read(self.id) {
            Ok(reader) => Ok(R::deserialize(reader)?),
            Err(e) if e.is_not_found() => Ok(Default::default()),
            Err(e) => Err(e.into())
        }
    }

    fn write(&mut self, index_document: IndexDocument) -> Result<()> {
        Ok(R::serialize(self.storage.write(self.id)?, &index_document)?)
    }
}

/// Index iterator, yeilding keys/value pairs.
///
/// This is an iterator of `(HashSet<Uuid>, T)`, because there can be multiple ids for the same
/// value in the index.
///
/// # Example
///
/// ```
/// # use dodo::{prelude::*, storage::Memory};
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// # use std::collections::HashSet;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, age: u64 }
/// # type NameIndex = Index<String, Memory, JsonSerializer>;
/// #
/// # fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #     let mut index = NameIndex::new(Memory::new());
/// #
/// let pairs : Vec<(HashSet<Uuid>, String)> = index.find_all()?
///                                                 .collect();
/// #
/// #     Ok(())
/// # }
/// ```
#[derive(Debug)]
pub struct IndexIterator<T> {
    iterator: vec::IntoIter<(HashSet<Uuid>, T)>,
}

impl<T> IndexIterator<T>
    where T: FromStr {
    fn new(index_document: IndexDocument) -> Result<Self> {
        let mut items = Vec::with_capacity(index_document.len());

        for (value, ids) in index_document.into_iter() {
            match T::from_str(&value) {
                Ok(value) => items.push((ids, value)),
                Err(_) => return Err(IndexError::syntax(format!("index value could not be parsed : {}", &value)))
            }
        }

        Ok(Self {
            iterator: items.into_iter()
        })
    }
}

impl<T> Iterator for IndexIterator<T> {
    type Item = (HashSet<Uuid>, T);

    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next()
    }
}
