//! Storage backends.

use std::io::{Read, Write};

use uuid::Uuid;

#[cfg(feature = "directory")]
pub use directory::*;
pub use error::*;
pub use memory::*;

#[cfg(feature = "directory")]
mod directory;
mod error;
mod memory;

/// Bytes storage backend.
///
/// Each entry is identified by a unique [id](https://docs.rs/uuid/*/uuid/struct.Uuid.html).
/// Storage implementations may choose to lock entries while they are read or written to.
pub trait Storage {
    /// Reader.
    type Read: Read;
    /// Writer.
    type Write: Write;
    /// Iterator.
    type Iterator: Iterator<Item=Result<Uuid>>;

    /// Create a new entry in this storage, returning his id and a writer.
    ///
    /// Returned id is guaranteed to be unique (inside this specific instance of storage, that is).
    fn new(&mut self) -> Result<(Uuid, Self::Write)>;

    /// Provide a reader to an entry, if it exists.
    fn read(&self, entry: Uuid) -> Result<Self::Read>;

    /// Provide a writer to an entry. If it exists, this writer overrides (and truncates) the bytes.
    /// If it doesn't exists, a new entry is created.
    fn write(&mut self, entry: Uuid) -> Result<Self::Write>;

    /// Provide a writer to an entry, only if it exists. This writer override (and truncates) the
    /// bytes.
    fn overwrite(&mut self, entry: Uuid) -> Result<Self::Write>;

    /// Delete an entry.
    ///
    /// This does not fail if the entry doesn't exists. Instead, this returns a boolean : if true,
    /// the entry was deleted, and if false, the entry was not found.
    fn delete(&mut self, entry: Uuid) -> Result<bool>;

    /// Empties this storage.
    ///
    /// Everything in this storage will be deleted. Use at your own risks.
    ///
    /// This does not fail if the storage was already empty.
    fn clear(&mut self) -> Result<()>;

    /// Provide an iterator listing the entries currently in this storage.
    ///
    /// The returned iterator is lazy and doesn't lock anything, meaning returned entries might not
    /// even exist when consumed.
    fn iter(&self) -> Result<Self::Iterator>;
}