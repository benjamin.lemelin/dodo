//! # Dodo
//!
//! Dodo (pronounced `doe doe`) is a very basic persistence library designed to be a quick and easy
//! way to create a persistent storage. It uses [Serde] under the hood to perform serialization of
//! the persisted data.
//!
//! ## Getting Started
//!
//! First, create a struct implementing the [Entity], [Serialize] and [Deserialize] traits, which
//! can be done using derives. For the `Entity` derive to work, it must contain a field named `id`
//! of type `Option<Uuid>`.
//!
//! ```
//! # use dodo::prelude::*;
//! # use serde::{Deserialize, Serialize};
//! # use uuid::Uuid;
//! #[derive(Entity, Serialize, Deserialize)]
//! #[serde(rename_all = "camelCase")]
//! struct Person {
//!     id: Option<Uuid>,
//!     name: String,
//!     age: u64,
//! }
//! ```
//!
//! Then, create a [storage][Storage] for your data. You can implement your own, or use the
//! default [Directory] storage.
//!
//! ```
//! # use dodo::prelude::*;
//! #
//! # fn main() -> Result<(), Box<dyn std::error::Error>> {
//! #    let tempdir  = tempfile::tempdir()?;
//! #    let path  = tempdir.path();
//! let directory = Directory::new(path)?;
//! #    Ok(())
//! # }
//! ```
//!
//! Now, you must choose a [serializer][Serializer]. You can also implement your own, or
//! use the default [JsonSerializer]. When this is done, it is highly recommanded to create a type
//! definition for your [collection][Collection] using the chosen storage and serializer.
//!
//! ```
//! # use dodo::prelude::*;
//! # use serde::{Deserialize, Serialize};
//! # use uuid::Uuid;
//! #
//! # #[derive(Entity, Serialize, Deserialize)]
//! # #[serde(rename_all = "camelCase")]
//! # struct Person {
//! #     id: Option<Uuid>,
//! #     name: String,
//! #     age: u64,
//! # }
//! #
//! type PersonCollection = Collection<Person, Directory, JsonSerializer>;
//! ```
//!
//! Finally, create a [collection][Collection] and start using it.
//!
//! ```
//! # use dodo::prelude::*;
//! # use serde::{Deserialize, Serialize};
//! # use uuid::Uuid;
//! #
//! # #[derive(Debug, Entity, Serialize, Deserialize)]
//! # #[serde(rename_all = "camelCase")]
//! # struct Person {
//! #     id: Option<Uuid>,
//! #     name: String,
//! #     age: u64,
//! # }
//! #
//! # type PersonCollection = Collection<Person, Directory, JsonSerializer>;
//! #
//! # fn main() -> Result<(), Box<dyn std::error::Error>> {
//! #     let tempdir  = tempfile::tempdir()?;
//! #     let path  = tempdir.path();
//! #     let directory = Directory::new(path)?;
//! let mut collection = PersonCollection::new(directory);
//! let mut person = Person { id: None, name: "John Smith".into(), age: 42 };
//! collection.insert(&mut person)?;
//! #     Ok(())
//! # }
//! ```
//!
//! ## Example
//!
//! This short example demonstrate how to use Dodo to read and write entities to the disk.
//!
//! ```
//! use std::error::Error;
//!
//! use serde::{Deserialize, Serialize};
//! use uuid::Uuid;
//!
//! use dodo::prelude::*;
//!
//! #[derive(Debug, Entity, Serialize, Deserialize)]
//! #[serde(rename_all = "camelCase")]
//! struct Person {
//!     id: Option<Uuid>,
//!     name: String,
//!     age: u64,
//! }
//!
//! type PersonCollection = Collection<Person, Directory, JsonSerializer>;
//!
//! fn main() -> Result<(), Box<dyn Error>> {
//!     let path = tempfile::tempdir()?;
//!
//!     let mut person1 = Person { id: None, name: "John Smith".into(), age: 18 };
//!     let mut person2 = Person { id: None, name: "Mary Smith".into(), age: 42 };
//!
//!     let mut collection = PersonCollection::new(Directory::new(&path)?);
//!     collection.insert(&mut person1)?;
//!     collection.insert(&mut person2)?;
//!
//!     println!("{:?}", collection.find_all()?.filter(|person| person.age > 20).collect()?);
//!
//!     Ok(())
//! }
//! ```
//!
//! [Entity]: entity/trait.Entity.html
//! [Storage]: storage/trait.Storage.html
//! [Directory]: storage/struct.Directory.html
//! [Serializer]: serializer/trait.Serializer.html
//! [JsonSerializer]: serializer/struct.JsonSerializer.html
//! [Collection]: collection/struct.Collection.html
//! [Serde]: https://docs.serde.rs/serde/index.html
//! [Serialize]: https://docs.serde.rs/serde/trait.Serialize.html
//! [Deserialize]: https://docs.serde.rs/serde/de/trait.DeserializeOwned.html

#![doc(html_root_url = "https://docs.rs/dodo/0.3.1")]
#![deny(missing_debug_implementations)]
#![deny(missing_docs)]

#[cfg(feature = "derive")]
#[allow(unused_imports)]
#[macro_use]
extern crate dodo_derive;

#[doc(inline)]
pub use collection::{Collection, CollectionError, CollectionErrorKind, Cursor};
#[doc(inline)]
pub use index::{Index, IndexError, IndexErrorKind};
#[cfg(feature = "derive")]
pub use dodo_derive::*;
#[doc(inline)]
pub use entity::Entity;
#[cfg(feature = "json")]
pub use serializer::JsonSerializer;
#[doc(inline)]
pub use serializer::Serializer;
#[cfg(feature = "yaml")]
pub use serializer::YamlSerializer;
#[doc(inline)]
pub use storage::{Memory, Storage};
#[cfg(feature = "directory")]
pub use storage::Directory;
#[cfg(feature = "directory_locks")]
pub use storage::{LockedDirectory};

#[cfg(doctest)]
doc_comment::doctest!("../../README.md");

pub mod collection;
mod entity;
pub mod index;
pub mod serializer;
pub mod storage;
pub mod prelude;