//! Serializers.

use std::io;

use serde::{de::DeserializeOwned, Serialize};

pub use error::*;
#[cfg(feature = "json")]
pub use json::*;
#[cfg(feature = "yaml")]
pub use yaml::*;

mod error;
#[cfg(feature = "json")]
mod json;
#[cfg(feature = "yaml")]
mod yaml;

/// Serialize and deserialize data into writer and readers respectively.
pub trait Serializer {
    /// Serialize value into writer, consuming the writer.
    fn serialize<T, W>(writer: W, value: &T) -> Result<()>
        where T: Serialize + DeserializeOwned,
              W: io::Write;
    /// Deserialize value from reader, consuming the reader.
    fn deserialize<T, R>(reader: R) -> Result<T>
        where T: Serialize + DeserializeOwned,
              R: io::Read;
}