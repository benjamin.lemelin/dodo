//! Json Serializer (using [Serde](https://docs.serde.rs/serde_json/)).

use std::io;

use serde::{de::DeserializeOwned, Serialize};

use super::{Result, Serializer, SerializerError};

/// JSON serializer (using [Serde](https://docs.serde.rs/serde_json/)).
///
/// # Examples
///
/// ```
/// use dodo::prelude::*;
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, name: String, age: u64 }
/// #
/// # impl Person {
/// #    fn with_age(age : u64) -> Self { Self { id : None, name : "John Smith".into(), age }}
/// # }
///
/// type PersonCollection = Collection<Person, Directory, JsonSerializer>;
///
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #   let path  = tempfile::tempdir()?;
///     let directory = Directory::new(&path)?;
///     let mut collection = PersonCollection::new(directory);
///     Ok(())
/// }
/// ```
#[derive(Debug, Clone)]
pub struct JsonSerializer;

impl Serializer for JsonSerializer {
    fn serialize<T, W>(mut writer: W, value: &T) -> Result<()>
        where T: Serialize + DeserializeOwned,
              W: io::Write {
        serde_json::to_writer(&mut writer, value)?;
        writer.flush().map_err(From::from)
    }

    fn deserialize<T, R>(reader: R) -> Result<T>
        where T: Serialize + DeserializeOwned,
              R: io::Read {
        serde_json::from_reader(reader).map_err(From::from)
    }
}

impl From<serde_json::Error> for SerializerError {
    fn from(error: serde_json::Error) -> Self {
        use serde_json::error::Category;
        match error.classify() {
            Category::Syntax | Category::Data => Self::syntax(error),
            Category::Io | Category::Eof => Self::io(error)
        }
    }
}