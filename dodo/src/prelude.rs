//! Prelude. Re-exports most used structs and traits.

#[doc(no_inline)]
pub use crate::{Collection, Cursor, Entity, Index};
#[cfg(feature = "directory")]
#[doc(no_inline)]
pub use crate::Directory;
#[cfg(feature = "json")]
#[doc(no_inline)]
pub use crate::JsonSerializer;
#[cfg(feature = "yaml")]
#[doc(no_inline)]
pub use crate::YamlSerializer;