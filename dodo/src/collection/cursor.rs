use super::Result;

/// Cursor over the contents of a collection.
///
/// Cursors deal with errors in a nicer way than iterators. Instead of providing results when
/// filtering, cursors provide entities when possible. This doesn't mean that cursors
/// ignore errors: you still have to manage them when consuming the cursor.
///
/// ```
/// # struct Person { age: u64 }
/// # impl Person {
/// #    fn new() -> Self { Self { age : 42 } }
/// # }
/// # fn main() {
/// #   let collection : Vec<Result<Person,()>> = vec![Ok(Person::new()), Ok(Person::new())];
/// // This is what you would have to do if cursors provided "Results".
/// let persons : Vec<&Result<Person,()>> = collection
///     .iter()
///     .filter(|result| result.as_ref().map_or(false, |person| person.age > 20))
///     .collect();
/// # }
/// ```
///
/// ```
/// # use dodo::{prelude::*, storage::Memory};
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, age: u64 }
/// #
/// # impl Person {
/// #    fn new() -> Self { Self { id : None, age : 42 } }
/// # }
/// #
/// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
/// #
/// # fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #   let mut collection = PersonCollection::new(Memory::new());
/// //This is what you can do instead.
/// let persons = collection
///     .find_all()?
///     .filter(|person| person.age > 20)
///     .collect()?;
/// #    Ok(())
/// # }
/// ```
pub trait Cursor {
    /// Yielded items.
    type Item;

    /// Advances the cursor and returns the next element.
    ///
    /// Returns `Ok(None)` when iteration is finished.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::new())?;
    ///
    /// let mut cursor = collection.find_all()?;
    ///
    /// assert!(matches!(cursor.next(), Ok(Some(person))), "Should exist.");
    /// assert!(matches!(cursor.next(), Ok(None)), "Should not exist.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    fn next(&mut self) -> Result<Option<Self::Item>>;

    /// Skip the first `n` elements.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::new())?;
    /// collection.insert(&mut Person::new())?;
    ///
    /// let entities = collection.find_all()?.skip(1).collect()?;
    ///
    /// assert_eq!(entities.len(), 1, "Should yield only one entity.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn skip(self, nb_items: usize) -> Skip<Self>
        where Self: Sized {
        Skip::new(self, nb_items)
    }

    /// Searches for an element that satisfies a predicate.
    ///
    /// Advances the cursor until it finds a candidate. Returns `Ok(None)` if
    /// no candidate is found.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn with_age(age : u64) -> Self { Self { id : None, age } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::with_age(42))?;
    /// collection.insert(&mut Person::with_age(18))?;
    ///
    /// let mut cursor = collection.find_all()?;
    ///
    /// assert!(matches!(cursor.find(|person| person.age < 20), Ok(Some(person))), "Should exist.");
    /// assert!(matches!(cursor.find(|person| person.age < 20), Ok(None)), "Should not exist.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn find<F>(&mut self, mut filter: F) -> Result<Option<Self::Item>>
        where Self: Sized,
              F: FnMut(&Self::Item) -> bool {
        while let Some(entity) = self.next()? {
            if filter(&entity) {
                return Ok(Some(entity));
            }
        }
        Ok(None)
    }

    /// Filters the cursor using a predicate.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn with_age(age : u64) -> Self { Self { id : None, age } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::with_age(42))?;
    /// collection.insert(&mut Person::with_age(18))?;
    ///
    /// let entities = collection.find_all()?.filter(|person| person.age > 20).collect()?;
    ///
    /// assert_eq!(entities.len(), 1, "Should yield only one entity.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn filter<F>(self, filter: F) -> Filter<Self, F>
        where Self: Sized,
              F: FnMut(&Self::Item) -> bool {
        Filter::new(self, filter)
    }

    /// Maps the cursor using a mapper.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn with_age(age : u64) -> Self { Self { id : None, age } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::with_age(42))?;
    /// collection.insert(&mut Person::with_age(18))?;
    ///
    /// let entities = collection.find_all()?.map(|person| person.age).collect()?;
    ///
    /// assert!(entities.contains(&42), "Should contain person age.");
    /// assert!(entities.contains(&18), "Should contain person age.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn map<B, F>(self, filter: F) -> Map<Self, F>
        where Self: Sized,
              F: FnMut(Self::Item) -> B {
        Map::new(self, filter)
    }

    /// Consumes the cursor, only taking the first element.
    ///
    /// Returns `Ok(None)` if iteration is finished.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// collection.insert(&mut Person::new())?;
    ///
    /// let person = collection.find_all()?.first();
    ///
    /// assert!(matches!(person, Ok(Some(person))), "Should have yielded the entity");
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn first(mut self) -> Result<Option<Self::Item>>
        where Self: Sized {
        self.next()
    }

    /// Take the `nth` element.
    ///
    /// Returns `Ok(None)` if iteration is finished. This will not skip errors.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::new())?; //Position 0
    /// collection.insert(&mut Person::new())?; //Position 1
    ///
    /// let mut cursor = collection.find_all()?;
    /// assert!(matches!(cursor.nth(1), Ok(Some(person))), "Should yield the second person.");
    /// assert!(matches!(cursor.next(), Ok(None)), "Should have no one left.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn nth(&mut self, mut index: usize) -> Result<Option<Self::Item>>
        where Self: Sized {
        while let Some(entity) = self.next()? {
            if index == 0 {
                return Ok(Some(entity));
            }
            index -= 1;
        }
        Ok(None)
    }

    /// Consumes the cursor into a `Vec`, taking only `n` elements.
    ///
    /// If an error is encountered, this discards every element collected so far and returns
    /// only the error.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, name: String, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, name : "John Smith".into(), age : 42 }}
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::new())?;
    /// collection.insert(&mut Person::new())?;
    ///
    /// let persons : Vec<Person> = collection.find_all()?.take(1)?;
    ///
    /// assert_eq!(persons.len(), 1, "Should only be one person in the results.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn take(mut self, nb_items: usize) -> Result<Vec<Self::Item>>
        where Self: Sized {
        let mut entities = Vec::new();
        while let Some(entity) = self.next()? {
            entities.push(entity);
            if entities.len() >= nb_items { break; }
        }
        Ok(entities)
    }

    /// Consumes the cursor into a `Vec`.
    ///
    /// If an error is encountered, this discards every element collected so far and returns
    /// only the error.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, name: String, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn with_age(age : u64) -> Self { Self { id : None, name : "John Smith".into(), age }}
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// let mut person1 = Person::with_age(42);
    /// let mut person2 = Person::with_age(18);
    /// collection.insert(&mut person1)?;
    /// collection.insert(&mut person2)?;
    ///
    /// let persons : Vec<Person> = collection.find_all()?
    ///                                       .filter(|person| person.age > 20)
    ///                                       .collect()?;
    ///
    /// assert!(persons.contains(&person1), "Person1 should be in the results.");
    /// assert!(!persons.contains(&person2), "Person2 should not be in the results.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    fn collect(mut self) -> Result<Vec<Self::Item>>
        where Self: Sized {
        let mut entities = Vec::new();
        while let Some(entity) = self.next()? {
            entities.push(entity);
        }
        Ok(entities)
    }
}

/// Cursor that skips `n` elements.
///
/// # Example
///
/// ```
/// # use dodo::prelude::*;
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, age: u64 }
/// #
/// # impl Person {
/// #    fn new() -> Self { Self { id : None, age : 42 } }
/// # }
/// #
/// # type PersonCollection = Collection<Person, Directory, JsonSerializer>;
/// #
/// # fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #   let path  = tempfile::tempdir()?;
/// #    let directory = Directory::new(&path)?;
/// #    let mut collection = PersonCollection::new(directory);
/// #
/// let entities = collection.find_all()?
///                          .skip(1)
///                          .collect()?;
/// #
/// #     Ok(())
/// # }
/// ```
#[derive(Debug)]
pub struct Skip<C> {
    cursor: C,
    nb_items: usize,
}

impl<C> Skip<C> {
    fn new(cursor: C, nb_items: usize) -> Self {
        Self {
            cursor,
            nb_items,
        }
    }
}

impl<C> Cursor for Skip<C>
    where C: Cursor {
    type Item = C::Item;

    fn next(&mut self) -> Result<Option<Self::Item>> {
        self.cursor.nth(self.nb_items)
    }
}

/// Cursor filter.
///
/// Only yields elements that satisfies a predicate.
///
/// # Example
///
/// ```
/// # use dodo::{prelude::*, storage::Memory};
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, age: u64 }
/// #
/// # impl Person {
/// #    fn with_age(age : u64) -> Self { Self { id : None, age } }
/// # }
/// #
/// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
/// #
/// # fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #     let mut collection = PersonCollection::new(Memory::new());
/// #
/// let entities = collection.find_all()?
///                          .filter(|person| person.age > 20)
///                          .collect()?;
/// #
/// #     Ok(())
/// # }
/// ```
#[derive(Debug)]
pub struct Filter<C, F> {
    cursor: C,
    filter: F,
}

impl<C, F> Filter<C, F> {
    fn new(cursor: C, filter: F) -> Self {
        Self {
            cursor,
            filter,
        }
    }
}

impl<C, F> Cursor for Filter<C, F>
    where C: Cursor,
          F: FnMut(&C::Item) -> bool {
    type Item = C::Item;

    #[inline]
    fn next(&mut self) -> Result<Option<Self::Item>> {
        self.cursor.find(&mut self.filter)
    }
}

/// Cursor mapper.
///
/// Maps the values of the cursor into something else.
///
/// # Example
///
/// ```
/// # use dodo::{prelude::*, storage::Memory};
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, age: u64 }
/// #
/// # impl Person {
/// #    fn with_age(age : u64) -> Self { Self { id : None, age } }
/// # }
/// #
/// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
/// #
/// # fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #     let mut collection = PersonCollection::new(Memory::new());
/// #
/// let ages = collection.find_all()?
///                      .map(|person| person.age)
///                      .collect()?;
/// #
/// #     Ok(())
/// # }
/// ```
#[derive(Debug)]
pub struct Map<C, F> {
    cursor: C,
    map: F,
}

impl<C, F> Map<C, F> {
    fn new(cursor: C, map: F) -> Self {
        Self {
            cursor,
            map,
        }
    }
}

impl<B, C, F> Cursor for Map<C, F>
    where C: Cursor,
          F: FnMut(C::Item) -> B {
    type Item = B;

    #[inline]
    fn next(&mut self) -> Result<Option<Self::Item>> {
        self.cursor.next().map(|it| it.map(&mut self.map))
    }
}
