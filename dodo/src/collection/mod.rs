//! Collection of entities.

use std::{marker::PhantomData, vec};

use uuid::Uuid;

pub use cursor::*;
pub use error::*;

use crate::entity::Entity;
use crate::serializer::Serializer;
use crate::storage::{self, Storage};

mod cursor;
mod error;

/// Collection of entities, backed by provided storage.
///
/// See this [crate](../index.html) root documentation for more information.
///
/// # Example
///
/// ```
/// use dodo::prelude::*;
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, age: u64 }
/// #
/// # impl Person {
/// #    fn new() -> Self { Self { id : None, age : 42 } }
/// # }
///
/// type PersonCollection = Collection<Person, Directory, JsonSerializer>;
///
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #   let path  = tempfile::tempdir()?;
///     let directory = Directory::new(&path)?;
///     let mut collection = PersonCollection::new(directory);
///
///     let mut person = Person::new();
///     collection.insert(&mut person)?;
///
///     let entities = collection.find_all()?.collect()?;
///
///     println!("{:?}", entities);
///
///     Ok(())
/// }
/// ```
#[derive(Debug, Clone)]
pub struct Collection<T, S, R> {
    storage: S,
    _t: PhantomData<T>,
    _r: PhantomData<R>,
}

impl<T, S, R> Collection<T, S, R>
    where T: Entity,
          S: Storage,
          R: Serializer {
    /// Create a new collection, using provided storage.
    ///
    /// # Examples
    ///
    /// ```
    /// use dodo::prelude::*;
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    ///
    /// type PersonCollection = Collection<Person, Directory, JsonSerializer>;
    ///
    /// fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #   let path  = tempfile::tempdir()?;
    ///     let directory = Directory::new(&path)?;
    ///     let collection = PersonCollection::new(directory);
    ///     Ok(())
    /// }
    /// ```
    pub fn new(storage: S) -> Self {
        Self {
            storage,
            _t: PhantomData,
            _r: PhantomData,
        }
    }

    /// Find an specific entity, if it exists.
    ///
    /// Returns an error if not found.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let collection = PersonCollection::new(Memory::new());
    /// #
    /// #     let id = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    /// let person = collection.find(id);
    ///
    /// match person {
    ///     Ok(person) => println!("Found!"),
    ///     Err(e) if e.is_not_found() => println!("Not found!"),
    ///     Err(_) => println!("Other error!")
    /// }
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn find(&self, id: Uuid) -> Result<T> {
        let reader = self.storage.read(id)?;
        Ok(R::deserialize(reader)?)
    }

    /// Provide a cursor iterating through the entities.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let collection = PersonCollection::new(Memory::new());
    /// #
    /// let persons = collection.find_all()?.collect();
    ///
    /// println!("{:#?}", persons);
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn find_all(&self) -> Result<CollectionCursor<T, S, S::Iterator, R>> {
        CollectionCursor::new(&self.storage)
    }

    /// Insert entity into this collection.
    ///
    /// The collection always assigns a new id to the entity, even if it already has one. Thus, the
    /// entity need to be mutable when inserted.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// let mut person = Person::new();  //Required to be mutable
    /// collection.insert(&mut person)?; //Assign new id to the entity.
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn insert(&mut self, entity: &mut T) -> Result<()> {
        let (id, writer) = self.storage.new()?;
        let old_id = entity.id(); //Backup old id, in case anything goes wrong.
        entity.set_id(Some(id));
        match R::serialize(writer, entity) {
            Ok(_) => Ok(()),
            Err(e) => {
                entity.set_id(old_id); //Something did go wrong. Restore old id.
                Err(e.into())
            }
        }
    }

    /// Update entity in this collection to a new version.
    ///
    /// The entity must already exist in the collection and have an id.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// let mut person = Person::new();
    /// collection.insert(&mut person)?;
    ///
    /// person.age = 1337;
    /// collection.update(&person)?;
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn update(&mut self, entity: &T) -> Result<()> {
        let id = entity.id().ok_or(CollectionError::unidentified())?;
        let writer = self.storage.overwrite(id)?;
        Ok(R::serialize(writer, entity)?)
    }

    /// Update or insert entity into this collection.
    ///
    /// The entity is created if it doesn't exists, using the id currently assigned to it. You
    /// will receive an error if the entity doesn't have an id.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None,  age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// let mut person = Person::new();
    /// person.id = Some(Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap());
    /// collection.upsert(&mut person)?; //Doesn't exist ? No problem here!
    ///
    /// let mut person = Person::new();
    /// collection.insert(&mut person)?;
    /// person.age = 1337;
    /// collection.upsert(&mut person)?; //Already exist ? No problem too!
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn upsert(&mut self, entity: &T) -> Result<()> {
        let id = entity.id().ok_or(CollectionError::unidentified())?;
        let writer = self.storage.write(id)?;
        Ok(R::serialize(writer, entity)?)
    }

    /// Delete entity with provided id.
    ///
    /// This does not fail if the entity doesn't exist. Instead, this returns a boolean : if true,
    /// the entity was deleted, and if false, the entity was not found.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// let mut person = Person::new();
    /// collection.insert(&mut person)?; //Inserted here.
    /// assert!(collection.delete(person.id.unwrap())?); //Deleted here.
    ///
    /// let id : Uuid = Uuid::parse_str("78190929-3d84-4735-9e40-80e3cd5530e9").unwrap();
    /// assert!(!collection.delete(id)?); //Doesn't exist ? No error!
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn delete(&mut self, id: Uuid) -> Result<bool> {
        Ok(self.storage.delete(id)?)
    }

    /// Delete every entity in this collection.
    ///
    /// Everything in this collection will be deleted. Use at your own risks.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// collection.insert(&mut Person::new())?;
    /// collection.insert(&mut Person::new())?;
    ///
    /// collection.clear()?;
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn clear(&mut self) -> Result<()> {
        Ok(self.storage.clear()?)
    }
}

/// Collection cursor, yeilding all entities.
///
/// # Example
///
/// ```
/// # use dodo::prelude::*;
/// # use serde::{Deserialize, Serialize};
/// # use uuid::Uuid;
/// #
/// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
/// # #[serde(rename_all = "camelCase")]
/// # struct Person { id: Option<Uuid>, age: u64 }
/// #
/// # impl Person {
/// #    fn new() -> Self { Self { id : None, age : 42 } }
/// # }
/// #
/// # type PersonCollection = Collection<Person, Directory, JsonSerializer>;
/// #
/// # fn main() -> Result<(), Box<dyn std::error::Error>> {
/// #    let path  = tempfile::tempdir()?;
/// #    let directory = Directory::new(&path)?;
/// #    let mut collection = PersonCollection::new(directory);
/// #
/// let entities = collection.find_all()?
///                          .filter(|person| person.age > 20)
///                          .skip(1)
///                          .take(3)?;
/// #
/// #     Ok(())
/// # }
/// ```
#[derive(Debug)]
pub struct CollectionCursor<'a, T, S, I, R> {
    storage: &'a S,
    iterator: I,
    _t: PhantomData<T>,
    _r: PhantomData<R>,
}

impl<'a, T, S, R> CollectionCursor<'a, T, S, S::Iterator, R>
    where T: Entity,
          S: Storage,
          R: Serializer {
    fn new(storage: &'a S) -> Result<Self> {
        let iterator = storage.iter()?;
        Ok(Self {
            storage,
            iterator,
            _t: PhantomData,
            _r: PhantomData,
        })
    }

    /// Shuffles the order the entities are yielded.
    ///
    /// Use this if you need to shuffle the entire collection contents. When done this early, this
    /// shuffles ids, not entities, so it is pretty lightweight. Nevertheless, this still has some
    /// serious performance implications, as this collects every id in the storage. With a small
    /// collection, this shouldn't be a problem, but with a large one, this will allocate a
    /// substantial amount of memory.
    ///
    /// # Examples
    ///
    /// ```
    /// # use dodo::{prelude::*, storage::Memory};
    /// # use serde::{Deserialize, Serialize};
    /// # use uuid::Uuid;
    /// #
    /// # #[derive(Debug, Entity, Serialize, Deserialize, Eq, PartialEq)]
    /// # #[serde(rename_all = "camelCase")]
    /// # struct Person { id: Option<Uuid>, age: u64 }
    /// #
    /// # impl Person {
    /// #    fn new() -> Self { Self { id : None, age : 42 } }
    /// # }
    /// #
    /// # type PersonCollection = Collection<Person, Memory, JsonSerializer>;
    /// #
    /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
    /// #     let mut collection = PersonCollection::new(Memory::new());
    /// #
    /// let mut person1 = Person::new();
    /// let mut person2 = Person::new();
    /// collection.insert(&mut person1)?;
    /// collection.insert(&mut person2)?;
    ///
    /// let persons : Vec<Person> = collection.find_all()?
    ///                                       .shuffled()
    ///                                       .collect()?;
    ///
    /// assert!(persons.contains(&person1), "Person1 should be in the results.");
    /// assert!(persons.contains(&person2), "Person2 should be in the results.");
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    #[inline]
    pub fn shuffled(self) -> CollectionCursor<'a, T, S, vec::IntoIter<storage::Result<Uuid>>, R> {
        use rand::seq::SliceRandom;

        let Self { storage, iterator, .. } = self;

        let mut entries: Vec<storage::Result<Uuid>> = iterator.collect();
        entries.shuffle(&mut rand::thread_rng());

        CollectionCursor {
            storage,
            iterator: entries.into_iter(),
            _t: PhantomData,
            _r: PhantomData,
        }
    }
}

impl<'a, T, S, I, R> Cursor for CollectionCursor<'a, T, S, I, R>
    where T: Entity,
          S: Storage,
          I: Iterator<Item=storage::Result<Uuid>>,
          R: Serializer {
    type Item = T;

    #[inline]
    fn next(&mut self) -> Result<Option<Self::Item>> {
        let Self { storage, iterator, .. } = self;

        //Map UUID to Entity
        let into_entity = |result: storage::Result<Uuid>| -> Result<Self::Item> {
            result
                .map_err(From::from)
                .and_then(|id| {
                    storage.read(id).map_err(From::from)
                })
                .and_then(|reader| {
                    R::deserialize(reader).map_err(From::from)
                })
        };

        //Collection might be modified while we are iterating over it. If an entry has been deleted
        //before we have time to read it, we simply ignore it.
        let ignore_not_found = |result: &Result<Self::Item>| -> bool {
            match result {
                Err(e) if e.is_not_found() => false,
                _ => true
            }
        };

        iterator
            .next()
            .map(into_entity)
            .filter(ignore_not_found)
            .transpose()
    }
}