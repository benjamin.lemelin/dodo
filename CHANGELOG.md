# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.1] - 2021-05-04

### Changed

- Directory storage now uses buffered io, as it should have.
- Directory no longer lock files. If you still want file locks, use LockedDirectory.

### Fixed

- Fixed issue with new version of `syn` not exporting the `TokenStream` struct.

## [0.3.0] - 2020-10-31

### Added

- Collections, indexes and storages now indicate if they have deleted something when asked to. 
- Index now has his own error type instead of relying on the collection error type.
- Cursors can now map their values to something else.

### Changed

- The `Index.find` method now returns a `Result<Hashset<Uuid>>` instead of `Result<Option<Hashset<Uuid>>>` for better
  ergonomics.
- Error kinds no longer holds extra information about the error.

## [0.2.0] - 2020-10-26

### Added

- Indexes. Indexes are very basic *multi-maps* backed by a storage. They are usually paired with collections to make 
  some queries faster as they allow to quickly locate an entity id without having to search the whole collection.
- This changelog.

### Changed

- Repositories are now called Collections.
- Errors are now structs holding an error kind and an error source.
- Errors now boxes their internal representations.
- Most errors have been renamed, so there is less confusion between the different error types.
- Prelude is included regardless of the feature set.
- Directory and memory storages are now `Clone`.

### Removed

- Removed `thiserror` dependency. The `Error` trait is now implemented manually.

## [0.1.1] - 2020-05-20

### Changed

- Changed from `err-derive` to `thiserror` to generate `Error` trait implementations.
- Updated Logo to use vectorized fonts for better compatibility.

## [0.1.0] - 2020-03-21

### Added

- Repositories. Repositories are very basic persistence layer that expose a *CRUD* api : *Create*, *Read*, *Update* 
  and *Delete*.
- Initial implementation of the crate.

[0.3.1]: https://crates.io/crates/dodo/0.3.1
[0.3.0]: https://crates.io/crates/dodo/0.3.0
[0.2.0]: https://crates.io/crates/dodo/0.2.0
[0.1.0]: https://crates.io/crates/dodo/0.1.0
[0.1.1]: https://crates.io/crates/dodo/0.1.1